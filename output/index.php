<?php
require('dbconnect.php');
session_start();

// 訪問回数を取得
if (!isset($_SESSION['visited'])) {
  $_SESSION['visited'] = 0;
} else {
        if ($_SESSION['flag'] == 1) {
          unset($_SESSION['flag']);
          $visited = $_SESSION['visited'];
        }
        else {
          $visited = $_SESSION['visited'];
          $visited++;
          $_SESSION['visited'] = $visited;
        }
}

// 訪問回数が6回を越えた場合(ヒントを5回見た場合)
if ($_SESSION['visited'] >= 6) {
  header('Location: machigai.php');
  exit();
}

 ?>

<!DOCTYPE html>
<html lang="ja" dir="ltr">
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta charset="utf-8">
    <title>数学クイズ！</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
     integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
     crossorigin="anonymous">

    <link rel="stylesheet" href="css/style.css">
  </head>
  <body>
    <div class="main1">
      <p class="main-title">
        数式の法則クイズ！
      </p>
      <p class="main1-1">
        6 + 4 = 210
      </p>
      <p class="main1-1">
        9 + 2 = 711
      </p>

      <!-- ヒントを訪問回数に応じて表示 -->
      <?php $i = 0; ?>
      <?php while (($visited+1) != $i): ?>
      <p class="main1-1">
        <?php
        $question = $db->prepare('SELECT * FROM question WHERE question_id=?');
        $question->execute(array($i));
        $q = $question->fetch();
        print($q['question']);
        $i++;
         ?>
      </p>
      <?php endwhile; ?>

      <p class="main1-2">
        上式の関係が成り立つとき、?に入る数字はいくつでしょう？
      </p>
      <p class="main1-3">
        15 + 3 = ?
      </p>

      <!-- アクションを指示 -->
      <div class="main1-4">
        分かった人は下のボタンから回答しよう！
      </div>
      <div class="main1-4">
        <p>分からない人はヒントボタンを押そう！関係式が追加で表示されるよ！</p>
        <p style="color:red;">(あと<?php  print(5-$visited);?>回)</p>
        <?php if ($visited == 5): ?>
          <p style="color:red;">ここで回答してね</p>
        <?php endif; ?>
      </div>

      <!-- 操作を決めるボタン群 -->
      <div class="main1-5">

        <!-- 分かった場合解答入力ページへ -->
        <a href="answer.php"><button type="button" class="btn btn-primary">わかった！</button></a>

        <!-- 訪問回数が5回までは、訪問回数に応じてヒントを表示 -->
        <?php if ($visited < 5): ?>
          <a href="index.php"><button type="button" class="btn btn-danger">ヒント</button></a>
        <?php endif; ?>

        <!-- 答えを見る場合、解答ページへ -->
        <a href="machigai.php"><button type="button" class="btn btn-warning">答えを見る</button></a>
      </div>
    </div>

  </body>
</html>

<?php
require('dbconnect.php');
session_start();

// 問題のページに戻った時にヒントが表示されないための処置
$visited = $_SESSION['visited'];
unset($_SESSION['visited']);

// 今回のユーザーの記録を残す
$kirokus = $db->prepare('INSERT INTO kiroku SET hint=? ,answer=1');
$kirokus->execute(array($visited));
 ?>

 <!DOCTYPE html>
 <html lang="ja" dir="ltr">
   <head>
     <meta charset="utf-8">
     <title></title>
   </head>
   <body>
     <p>正解です！</p>

     <!-- 現段階のヒントでの正解率 -->
     <p>ヒント<?php print($visited); ?>回での正解率は
       <?php
       $kirokus = $db->prepare('SELECT AVG(answer) AS average FROM kiroku WHERE hint=?');
       $kirokus->execute(array($visited));
       $kiroku = $kirokus->fetch();
       print($kiroku['average']*100);
      ?>％です！
    </p>
    
    <p><a href="index.php">問題に戻る</a></p>
   </body>
 </html>

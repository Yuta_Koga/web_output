<?php
require('dbconnect.php');
session_start();

// 問題のページに戻った時にヒントが表示されないための処置
if ($_SESSION['visited'] <= 5) {
  $visited = $_SESSION['visited'];
} else {
  $visited = 5;
}
unset($_SESSION['visited']);

// 今回のユーザーの記録を残す
$kirokus = $db->prepare('INSERT INTO kiroku SET hint=? ,answer=0');
$kirokus->execute(array($visited));

 ?>
<!DOCTYPE html>
<html lang="ja" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>

    <!-- 解答と説明 -->
    <p>残念!正解は1218です！</p>
    <p>15-3=12</p>
    <p>15+3=18</p>
    <p>なので、1218です！</p>
    <p>他の式も全て同じ関係で成り立ってます。</p>
    <p>8 + 5 = 313</p>
    <p>5 + 2 = 37</p>
    <p>7 + 6 = 113</p>
    <p>9 + 8 = 117</p>
    <p>10 + 6 = 410</p>
    <br>

    <!-- 正解率を表示 -->
    <p>ちなみにヒント<?php print($visited); ?>回での正解率は

      <!-- ヒントの回数に応じた正解率を取得 -->
      <?php
      $kirokus = $db->prepare('SELECT AVG(answer) AS average FROM kiroku WHERE hint=?');
      $kirokus->execute(array($visited));
      $kiroku = $kirokus->fetch();
      print($kiroku['average']*100);
     ?>％です！
     
   </p>

   <!-- 問題へ戻る -->
   <p><a href="index.php">問題に戻る</a></p>

  </body>
</html>

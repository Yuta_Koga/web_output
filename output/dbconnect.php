<?php
try {
  // 通常の処理
  // PDOオブジェクトのインスタンス生成

  // localhost
  $db = new PDO ('mysql:dbname=my_output;host=127.0.0.1;charset=utf8', 'root', '');

} catch (PDOException $e) {
  // 例外処理
  // getMessage:例外の取得
  echo 'DB接続エラー' . $e->getMessage();
}
 ?>

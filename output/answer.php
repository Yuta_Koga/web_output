<?php
require('dbconnect.php');
session_start();

// 問題に戻っても、ヒントは追加で表示されないための処置
// trueならば、index.phpで$_SESSION['visited']は更新されない
$_SESSION['flag'] = 1;
 ?>

 <!DOCTYPE html>
 <html lang="ja" dir="ltr">
   <head>
     <meta charset="utf-8">
     <title>答えの入力</title>
     <link rel="stylesheet" href="css/style_answer.css">
   </head>
   <body>
     <p>解答をどうぞ！(半角数字で入力してね)</p>

     <!-- 回答の送信ボタン -->
     <form class="" action="judgement.php" method="get">
       <input class="form-control" style="padding:5px" type="text" placeholder="回答" name="answer">
       <input type="submit" name="" value="送信">
     </form>

     <!-- 問題に戻るボタン -->
     <p><a href="index.php">問題に戻る</a></p>
   </body>
 </html>
